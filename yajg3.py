#!/usr/bin/env python
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# 
# Copyright (c) 2014, ap-Codkelden <maladi at gmail dot com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
TODO:

1. Функцию, кторая будет возвращать время задержки
2. Чтобы после неверного ввода логина папка удалялась
3. Заменить обработку постов в файле JSON обработкой БД. Должно стать быстрее.
sys.stdout.write("Download progress: %d%%   \r" % (progress) )
sys.stdout.flush()
"""

import argparse
import lxml.html
import re
import codecs
import requests
import time
import getpass
import logging
import json
import os.path
from random import uniform, shuffle
import glob
import os
import sys
import pprint 

"""
TODO:
Переписать нормально сохранение post_numbers, чтоб 
оттуда брать нормальные номера
+ переименовать переменную номера сообщения перехода """

# pp.pprint(stuff)
pp = pprint.PrettyPrinter(indent=4)


import logging

def query_yes_no(question, default="no"):
    """Функция запроса удаления блога у пользователя.
    """
    valid = {"yes":True, "y":True, "no":False, "n":False}
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("Invalid, default answer is: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "\
                             "(or 'y' or 'n').\n")

class SimpleStore:
    def __init__(self, uname, **kwargs):
        self.end_reached = False
        # здесь 
        # last_mid - старшее сообщение #200
        # first_mid - младшее сообщение #100
        self.last_mid = ''
        self.first_mid = ''
        self.dirname = kwargs['dirname']
        self.juick = kwargs['juick']
        self.uname = uname
        self.args = kwargs['args']
        logging.debug ("Class %s \nargs: %s" % (SimpleStore.__name__, self.args))

    def previous_data_check(self):
        if not self.args.new:
            if os.path.isfile(os.path.join(self.dirname, 'savednums')):
                with open(os.path.join(self.dirname, 'savednums'), 'r') as f:
                    mids = f.read().split()
                    self.last_mid = max(mids)
                    self.first_mid = min(mids)
                    logging.info("Fist and last mid are: #{1} and #{0} simultaneously.".format(self.last_mid,self.first_mid))
            if not os.path.isfile(os.path.join(self.dirname, 'posts.json')):
                self.juick.write_json(os.path.join(self.dirname, 'posts'),[],'w')
        else:
            self.juick.delete_stored_files(self.dirname)
            self.last_mid = ''
            # open(os.path.join(self.dirname, 'savednums'),'a').close()
            self.juick.write_json(os.path.join(self.dirname, 'posts'),[],'w')

    def store_posts (self, mid_list):
        """mid_list будет содержать номера постов, 
        """
        # print(mid_list)
        with open(os.path.join(self.dirname, 'post_numbers'),'a') as f:
            for item in mid_list:
                f.write ('{0}\n'.format(str(item)))
                

    def fetch_unauthorized_mids(self):
        """Функция сбора номеров в режиме без авторизации.
        """
        if self.last_mid!='':
            next_page_mid=self.last_mid
        if self.args.new:
            first_page = True
        else:
        #elif self.last_mid!='':
            first_page = False
        end_blog = False
        post_store = []
        next_page_mid = ''
        init_url = 'http://api.juick.com/messages?user_id={0}'.format(self.juick.get_uid(self.uname))
        while not end_blog:
            try:
                if first_page:
                    url = init_url
                else: 
                    url = init_url+'&before_mid={0}'.format(next_page_mid)
                print(url)
                post_list = []
                response = json.loads(((self.juick._session.get(url)).content.replace(b'\n', b'').replace(b'\r', b'')).decode(encoding='UTF-8'))
                pp.pprint(self.last_mid)
                pp.pprint(self.first_mid)

                for item in response:
                    if self.last_mid!='' or self.first_mid!='':
                        if self.last_mid!='' and (int(self.last_mid) < int(item['mid'])):
                            post_list.append(item['mid'])
                            logging.info('New junior mid #{0} added.'.format(item['mid']))
                            if not os.path.isfile(os.path.join(self.dirname, 'endreached')):
                                next_page_mid = self.first_mid
                            else:
                                print("No more messages.")
                                end_blog = True
                                exit
                        elif self.first_mid!='' and (int(self.first_mid) > int(item['mid'])):
                            post_list.append(item['mid'])
                            logging.info('New elder mid #{0} added.'.format(item['mid']))
                        next_page_mid = response[-1]['mid']
                    else:
                        #elif os.path.isfile(os.path.join(self.dirname, 'endreached')):
                        #    end_blog = True
                        #    exit
                        post_list.append(item['mid'])
                        next_page_mid = response[-1]['mid']
                self.store_posts(post_list)
                post_store.extend(post_list)
                first_page = False
                sleep_delay = uniform(self.args.initdelay * 1.2, \
                    self.args.initdelay * 1.6)
                if self.args.pauses:
                    logging.debug("Paused for {0:.2f} s".format(sleep_delay))
                time.sleep(sleep_delay)
            except ValueError:
                end_blog = True
                logging.info("End of {0} blog reached".format(self.uname))
                open(os.path.join(self.dirname, 'endreached'), 'a').close()
            except KeyboardInterrupt:
                 os.sys.exit(-1)

        if not self.args.new:
            if os.path.isfile(os.path.join(self.dirname, 'savednums')):
                with open(os.path.join(self.dirname, 'savednums'),'r') as stored_post_file:
                    saved_mids=[int(mid) for mid in stored_post_file.read().split()]
                # print("SAVEDMIDS",saved_mids)
                post_store =  list(set(post_store) - set(saved_mids))
                if len(post_store) == 0:
                    msg_string = "No mids to fetch, exit"
                    logging.info(msg_string)
                    os.sys.exit()
            else:
                print("No saved mids file found.")
        for mid in post_store:
            yield mid

class DeleteBlog:
    """Класс удаления записей. 
    Аргументы:
        dirname (str) - имя каталога, куда происходит сохранение рабочих файлов.
    """

    def __init__(self, dirname):
        self.dirname = dirname

    def _get_numbers(self, filename):
        """Функция получения списка номеров постов (либо списка номеров постов 
        блога, либо списка сохраненных постов, в зависимости от значения параметра 
        --stored-mids).
        Аргументы:
            filename (str) - имя файла с номерами постов ("post_numbers.json"), либо 
            фала с номерами сохраненных постов ("savednums").
        Возвращает список постов, если они есть, или пустой список, если таких постов 
        нет, или же если файл с постами не найден.
        """
        if os.path.isfile(os.path.join(self.dirname, filename)):
            try:
                with open(os.path.join(self.dirname, filename))  as f:
                    mids = f.read()
                    mids = mids.split()
                    if len(mids) > 0:
                        return mids
                    else:
                        return []
            except Exception as e:
                raise e
        else:
            return []

    def generate_post_params(self, stored_mids):
        """Функция генерации строки для передачи сервверу Juick вида 'D #12345'. 
            Аргументы:
                stored_mids - список постов, возвращённых функцией _get_numbers 
                этого класса.
            Возвращает генератор строк.
        """
        if stored_mids:
            saved_mids = [x[0] for x in json.load(open(os.path.join(self.dirname, 'post_numbers.json')))]
        else:
            saved_mids = self._get_numbers('savednums')
        deleted_mids = self._get_numbers('deletednums')
        mids =  list(set(saved_mids) - set(deleted_mids))
        if len(mids) == 0:
            logging.info ("Nothing to delete.")
            os.sys.exit()
        shuffle(mids)
        for i in mids:
            yield "D #{0}".format(i)

class getImage:
    """Класс загрузки изображений. 
    Процедуры:
        getImage - для загрузки изображений, опубликованных средствами 
            Juick
        getFileMIME - для определения типа данных стороннего файла и его загрузки,  
            если он является изображением.
        get_juick_image - для скачивания изображений, вложенных в посты и 
            комментарии 
        get_external_image - для скачивания изображений по ссылкам на них, 
            вложенным в посты и комментарии 
    Переменные:
        __CONTENT_TYPES - список типов MIME согласно RFC 2046

    """

    def __init__(self, **kwars):
        # список MIME-типов
        self.__CONTENT_TYPES = ('image/gif',
            'image/jpeg', 
            'image/pjpeg', 
            'image/png', 
            'image/svg+xml',)
        self.pauses = kwars['pauses']

        self.full_juick_image_link = \
            lambda s: 'https://i.juick.com/p/{0}'.format(s.split('/')[-1])
        self.url_patt = re.compile('(https?\:[//].*?[.](jpe{0,1}g|png|gif|bmp|svg)\:?(large)?)')

    def get_juick_image(self, item, delay, imagedir):
        """Функция скачивает изображения, вложенные в посты и комментарии 
        пользователя.
        Аргументы:
            item - тело поста или комментария к нему
            delay - значение нижней границы задержки
            imagedir - папка для сохранения изображений Juick
        При возникновении ошибки возбуджается стандартное исключение.
        """
        try:
            if 'photo' in item:
                image_url = item['photo']['medium']
                path = os.path.join(imagedir, image_url.split('/')[-1])
                logging.info ("Try retrieve image from #{0}".format(image_url.split('/')[-1]))
                if self.getImage(self.full_juick_image_link(image_url), \
                    path, delay) != 0:
                        logging.info ("Success")
        except Exception as e:
            raise e

    def get_external_image(self, item, delay, aliendir):
        """Функция скачивает изображения по ссылкам на них, вложенным в посты и
        комментарии пользователя.
        Аргументы:
            item - тело поста или комментария к нему
            delay - значение нижней границы задержки
            imagedir - папка для сохранения сторонних изображений 
        При возникновении ошибки возбуджается стандартное исключение.
        """
        matches = re.findall(self.url_patt, item['body'])
        if len(matches) > 0:
            for img_url in matches:
                try:
                    self.getFileMIME(img_url[0], \
                        os.path.join(aliendir,str(item['mid'])+'_'+img_url[0].split('/')[-1]), \
                        delay)
                except Exception as e:
                    raise e

    def getFileMIME(self, url, alien_name, delay):
        """Процедура определяет MIME-тип данных файла стороннего изображения. В 
        случае, если тип данных содержится в кортеже __CONTENT_TYPES, вызывается 
        процедура getImage этого класса.
        В случае возникновения ошибки исключения не возбуждается, а выводится 
        сообщение об ошибке.
        """
        try:
            r = requests.get(url)
            mime = r.headers['content-type']
            if mime in self.__CONTENT_TYPES:
                logging.info ("Try retrieve image from URL {0}".format(url))
                if self.getImage(url, alien_name, delay) == 0:
                    logging.info ("Success")
                else:
                    logging.warning("Image loading fail:(")
        except Exception as e:
            logging.debug('External image downloading error: %r' % e)
            logging.info("Downloading image error, skipped.")
            pass

    def getImage(self, url, path, delay):
        """Процедура загрузки изображения. 
        Агрументы:
            url - URL изображения
            path - путь сохранения изображения
            delay - время задержки
        В случае ошибки возбуджается стандартное исключение.
        Возвращает: 
            0 - если файл загружен успешно
            1 - если произошла ошибка
        """
        sleep_delay = uniform(delay, delay*1.4)
        if self.pauses:
            logging.debug ("Delay before image download for {0:.2f} s".format(delay))
        time.sleep(delay)
        r = requests.get(url, stream=True)
        try:
            if (not os.path.isfile(path)) or (os.path.isfile(path) and os.path.getsize(path) > 0):
                if r.status_code == 200:
                    with open(path, 'wb') as f:
                        for chunk in r.iter_content(1024):
                            f.write(chunk)
            else:
                logger.info("File already exists, skipped.")
            return 0
        except Exception as e:
            # raise e
            logging.warning ("Downloading image error:(")
            return 1

class Juick:
    """Класс получения записей авторизованного пользователя.
    Объекты класса:
        _session - объект requests.Session(), основной объект для операций 
        взаимодействия с сервером
    """

    def __init__(self):
        self._session = requests.Session()

    def write_json(self, name, data, mode):
        """Процедура записи файлов в формате JSON.
        Аргументы:
            name - имя файла (без расширения)
            data - данные
            mode - режим (принимает значения w или a)
        """
        with codecs.open('{0}.json'.format(name), mode, 'utf-8') as f:
            json.dump(
                data, f,
                ensure_ascii=False)

    def get_uid(self, uname):
        """Процедура получения UID.
        Аргументы:
            uname - имя пользователя (логин) Juick
        Возвращает:
            UID
        """
        try:
            return (self._session.get('http://api.juick.com/users?uname={0}'.format(uname))).json()[0]['uid']
        except ValueError as e:
            print('User not found.')
            os.sys.exit()
        except Exception as e:
            raise e
        

    def auth(self, uname, password):
        """Процедура авторизации пользователя.
        Аргументы:
            uname - имя пользователя (логин)
            password - пароль
        Возвращает:
            1 - в случае, если процедура авторизации прошла успешно
            0 - если авторизация не пройдена, возвращает и предлагает либо 
                попробовать снова, либо собрать посты, которые доступны без 
                авторизации.
        """
        try:
            self._session.post('https://juick.com/login',
                            params={'username': uname, 'password': password})
            self._session.auth = uname, password
            if len(self._session.cookies) == 0:
                print("Not authorized, try collect public posts only or try again.")
                return 1
            else:
                print("Authorized.")
                return 0
            logging.debug('Got cookies: %r' % self._session.cookies)
        except Exception as e:
            raise e

    def thread(self, mids, delay, pauses):
        """Процедура возвращает генератор кортежа вида:
            (URL треда в API, признак friends, признак readonly), 
            где:
            признак friends и признак readonly - принимают значения 
                0 или 1 в зависимости от наличия или отсутствия таковых.
        Аргументы:
            mids - список номеров постов 
            delay - значение задержки
        """
        threads_data = [('https://api.juick.com/thread?mid={0}'.format(mid[0]),mid[1],mid[2]) for mid in mids]
        for thread in threads_data:
            sleep_delay = uniform(delay, delay*1.4)
            if pauses:
                logging.debug("Paused for {0:.2f} s".format(sleep_delay))
            time.sleep(sleep_delay)
            yield (thread)

    def append_json(self, filename, data):
        """Процедура добавления сборанных постов в файл 'posts.json'. Читает уже 
        существующий файл (параметр filename) с массивом сохраненных постов (он 
        может быть пустым), затем реверсирует его, добавляет в конец этого 
        массива вновь собранные посты (параметр data), вновь реверсирует уже 
        дополненный массив, и перезаписывает файл с сохраненными постами.
        Аргументы:
            filename - имя файла
            data - данные JSON
        """
        data.reverse()
        with open(filename, mode='r') as f:
            j = json.load(f)
            j.reverse()
            for d in data:
                j.append(d)
            j.reverse()
        with open(filename, mode='wb') as f:
            s = json.dumps(j, ensure_ascii=False)
            f.write(s.encode('utf-8'))

    def compare (self, wrote_mid, stored_mid):
        """Функция сравнения списков сохраненных и записанных сообщений.
        Аргументы:
            wrote_mid - список номеров уже сохраненных постов
            stored_mid - список сохраненных номеров ещё незаписанных постов
        Возвращает:
            Список несохраненных номеров, если такие есть, если их нет, 
            возвращается список номеров уже сохраненных постов
        В случае ошибки возбуждается стандартное исключение.
        """
        try:
            if wrote_mid != None:
                return ([i for i in stored_mid if i[0] in list(set([i[0] for i in stored_mid]) - set(wrote_mid))])
            else:
                return stored_mid
        except Exception as e:
            print('error in Compare', e)
            raise e

    def delete_stored_files (self, dirname):
        """Функция удаления сохраненных в предыдущей сессии работы скрипта 
        файлов (если таковые существуют):
            - файл лога 
            - файлы JSON
            - всё файлы сохраненных номеров постов
            - файл признака конца блога 'endreached'
        В случае возникновения ошибки возбуждается стандартное исключение.
        """
        try:
            for fle in glob.glob(os.path.join(dirname, '*.json')):
                if os.path.exists(fle):
                    os.remove(fle)
            for fle in glob.glob(os.path.join(dirname, '*nums')):
                if os.path.exists(fle):
                    os.remove(fle)
            for fle in glob.glob(os.path.join(dirname, '*.log')):
                if os.path.exists(fle):
                    os.remove(fle)
            if os.path.exists(os.path.join(dirname, 'endreached')):
                os.remove(os.path.join(dirname, 'endreached'))
            if os.path.exists(os.path.join(dirname, 'post_numbers')):
                os.remove(os.path.join(dirname, 'post_numbers'))
        except Exception as e:
            raise e
        
    def preview_mid(self, dirname):
        """Функция открывает файл номеров сохраненных постов `savednums`, если 
        он существует есть, и пытается считать из него список сохраненных 
        постов.
        Аргументы:
            dirname (str) - имя каталога, куда происходит сохранение рабочих файлов.
        В случае возникновения ошибки возбуждается стандартное исключение.
        """
        try:
            if os.path.isfile(os.path.join(dirname,'savednums')):
                with open(os.path.join(dirname,'savednums')) as f:
                    sav_nums = f.read()
                return sav_nums.split()
        except Exception as e:
            # raise e
            print("Error on preview_mid", e)
            return []

    def fetch_numbers(self, url, **kwargs):
        """Функция сбора номеров постов из веб-интерфейса.
        Получает странцу с постами и парсит её в поисках номеров. При достижении 
        конца страницы извлекает номер последнего поста на этой странице из 
        ссылки "Читать дальше →" и переходит к ней, и так до момента, пока на 
        странице не будет найдена ссылка перехода к предыдущим постам (в этом 
        случает создает пустой файл с названием 'endreached', и вместо номера 
        поста для перехода по ссылке будет возвращено False).
        Посты с каждой страницы сохраняются в файл через промежуток в N постов, 
        где N по умолчанию равно 5 и может быть указано в параметре командной 
        строки --save-each.

        Аргументы:
            dirname - каталог пользователя
            eof_reached - признак сохранения блога от до начала
            delay - начальное значение задержки

        Возвращает:
            Список номеров постов с признаками 'readonly' и 'friends'.
        """
        logging.debug('Juick.%s%r' % (url, kwargs))
        # Проверка на наличие сохраненных номеров постов 
        if os.path.isfile(os.path.join(kwargs['dirname'], 'post_numbers.json')):
            is_not_first_run = True
            print('found numbers file')
            try:
                with open(os.path.join(kwargs['dirname'], 'post_numbers.json')) as f:
                    post_store = json.load(f)
                first_mid = min([x[0] for x in post_store])
                last_mid = max([x[0] for x in post_store])
                logging.debug("FIRST saved mid# is: %s" % first_mid)
                logging.debug("LAST saved mid# is: %s" % last_mid)
            except Exception as e:
                print(e)
        else:
            is_not_first_run = False
            post_store = []
            first_mid = last_mid = 0
        last_post_number = True
        mid_count = 0
        mid = kwargs['begin_from']
        jump = False
        while True:
            try:
                posts = []
                logging.debug("Keyword args is: %s" % str(kwargs))
                dynamic_url = ('{0}?before={1}'.format(url,mid))
                if jump:
                    jump = False
                while True:
                    try:
                        sleep_delay = uniform(kwargs['delay'], kwargs['delay']*1.4)
                        if kwargs['pauses']:
                            logging.debug("Paused for {0:.2f} seconds.".format(kwargs['delay']))
                        time.sleep(sleep_delay)
                        h = lxml.html.fromstring(self._session.get(dynamic_url).text)
                        break
                    except requests.exceptions.ConnectionError:
                        pass
            except Exception as e:
                # raise e
                print(e)
                pass

            for post in h.xpath('//section[@id="content"]/article'):
                if (kwargs['eof_reached'] and (int(post.xpath('@data-mid')[0]) > int(last_mid))) or \
                    (not kwargs['eof_reached'] and \
                    (int(post.xpath('@data-mid')[0]) > int(last_mid) or \
                    int(post.xpath('@data-mid')[0]) < int(first_mid))):
                    for tag in post.xpath("header[@class='u']"):
                        is_friends, is_readonly = (1 if 'friends' in tag.text_content() else 0), (1 if 'readonly' in tag.text_content() else 0)
                        posts.append([
                            post.xpath('@data-mid')[0],
                            is_friends, is_readonly])
                        logging.info("[%(asctime)s] Mid #{0} fetched.".format(post.xpath('@data-mid')[0]))
                else:
                    if int(post.xpath('@data-mid')[0]) == int(last_mid):
                        print("Возможно, достигнута верхняя граница предыдущей выборки, переход.")
                        mid = first_mid
                        logging.debug("[%(asctime)s] mid: ".format(mid))
                        jump = True
            if len(h.xpath("//section[@id='content']/p/a/@href")) > 0:
                if not jump:
                    logging.debug("[%(asctime)s] Jumping to elder mid: ".format(jump))
                    mid = last_post_number = \
                        h.xpath("//section[@id='content']/p/a/@href")[0].split('=')[1]
            else:
                open(os.path.join(kwargs['dirname'], 'endreached'), 'a').close()
                last_post_number = False
            post_store.extend(posts)
            mid_count += 1
            if mid_count == kwargs['save_each']:
                self.write_json(os.path.join(kwargs['dirname'],'post_numbers'), post_store, 'w')
                mid_count = 0
            if not last_post_number: 
                self.write_json(os.path.join(kwargs['dirname'],'post_numbers'), post_store, 'w')
                break 
        print("{0} mid(s) fetched.".format(len(post_store)))
        print(post_store)
        return post_store

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('username',
                        help='Target user name (WARNING: case sensitive)')

    #parser.add_argument('-v', '--verbose',
    #                    help='Increase output verbosity',
    #                    action='count',
    #                    default=0)
    
    parser.add_argument('--auth',
                        help='Log in as target user',
                        action='store_true')

    parser.add_argument('--delete', '-d',
                        help='Delete blog, see README for details',
                        action='store_true')

    parser.add_argument('--new',
                        help='Start fetching from clean slate',
                        action='store_true')

    parser.add_argument('--stored-mids',
                        help='Delete not by stored posts (as default), but by stored mids',
                        action='store_true')

    parser.add_argument('--juick-images', 
                        help='Save images from threads. See README for details.',
                        action='store_true')

    parser.add_argument('--external-images', 
                        help='Save images from threads. See README for details.',
                        action='store_true')

    parser.add_argument("--delay", type=float, default=0.7, action="store",
                        dest='initdelay', help="Initial delay value in seconds (positive float number greater or equal 0.5).")

    parser.add_argument("--save-each", type=int, default=5, action="store",
                        dest='save_each', help="Save fethed mids every N fetched")

    parser.add_argument("--fetch", action="store_true",
                        help="Fetch mids only")

    parser.add_argument("--pauses", action="store_true",
                        help="Show pauses duration")

    parser.add_argument("--begin-from", type=str, action="store",  default='',
                        dest='begin_from', help="number of mid to start from (experimental)")

    args = parser.parse_args()
    print(args)

    if args.initdelay >= 0.7: 
        DELAY = args.initdelay
    else: 
        print("Delay value is invalid, revert to default (0.5 s)")
        DELAY = 0.7

    if args.save_each >= 5: 
        SAVE_EACH = args.save_each
    else: 
        print("Delay value is invalid, revert to default (5 mid)")
        SAVE_EACH = 5


    dirname = uname = args.username
    if args.auth:
        dirname += '.private'

    juick = Juick()
    juick.name = uname

    if not os.path.exists(dirname):
        os.makedirs(dirname)


    #--------------------------------------------------------------------------
    #  Логирование, уровень DEBUG пишется в файл, 
    #  уровень INFO выводится в stderr
    #--------------------------------------------------------------------------
    # настройка файла лога
    logging.basicConfig(level=logging.DEBUG, 
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s', 
                        datefmt='%d.%m %H:%M', 
                        filename = os.path.join(dirname, '{0}.log'.format(juick.name)), 
                        filemode='w')

    # обработчик  INFO сообщений или выше в  sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # немного более простой формат
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    # добавляем обработчик к обработчику высшего уровня 
    logging.getLogger('').addHandler(console)
    # теперь можно писать в лог
    # logging.info('Jackdaws love my big sphinx of quartz.')

    # Тут можно ещё кучку логгеров запилить:
    # logger1 = logging.getLogger('myapp.area1')
    # logger2 = logging.getLogger('myapp.area2')



    if args.juick_images or args.external_images:
        image_downloader = getImage(pauses=args.pauses)

    if args.juick_images:
        imagedir_name = os.path.join(dirname,'juick-images')
        if not os.path.exists(imagedir_name):
            os.makedirs(imagedir_name)
    if args.external_images:
        external_imagedir_name = os.path.join(dirname,'external-images')
        if not os.path.exists(external_imagedir_name):
            os.makedirs(external_imagedir_name)

    if args.auth:
        try:
            if args.delete:
                print("start delete")
                if args.fetch:
                    print("Cannot fetch and delete simultaneously.")
                    os.sys.exit()
                if (not args.new) and (not (args.juick_images or args.external_images)):
                    print('w/o add keys')
                    password = getpass.getpass('@{0} password:\n'.format(uname))
                    if juick.auth(uname, password) == 0:
                        stored_file_mid = stored_file_post = False
                        print("Proceed to delete.")
                        # тут пилим выбор файла в зависимости от того, что выбрал
                        # юзер - сохраненные посты или сохраненные номера
                        if not args.stored_mids:
                            stored_file_mid = True if (os.path.isfile(os.path.join(dirname, 'savednums'))) else False
                            file_not_found_msg = 'posts'
                        else:
                            stored_file_post = True if (os.path.isfile(os.path.join(dirname, 'post_numbers.json'))) else False
                            file_not_found_msg = 'mids ("post_numbers.json")'
                        if (stored_file_mid or stored_file_post):
                            deleter = DeleteBlog(dirname)
                            mids = deleter.generate_post_params(args.stored_mids)
                            if query_yes_no("Are you sure to delete your Juick blog, %USERNAME%?", default="no"):
                                open(os.path.join(dirname,'deletednums'), 'a').close()
                                for mid in mids:
                                    print('mid', mid)
                                    sleep_delay = uniform(DELAY*1.2, DELAY*1.6)
                                    if args.pauses:
                                        logging.debug("Paused for {0:.2f} s".format(sleep_delay))
                                    time.sleep(sleep_delay)
                                    try:
                                        midnum = mid.split("#")[-1]
                                        mid2del = {}
                                        mid2del['attach'] = ""
                                        mid2del['place_id'] = "0"
                                        mid2del['body'] = mid
                                        # !!! следующая строка удаляет сообщения !!!
                                        r = juick._session.post('http://juick.com/post2', data=mid2del)
                                        logging.info("Deleted mid #{0}".format(midnum))
                                        # а тут была эмуляция удаления
                                        # print("Deleted mid #{0}".format(midnum))
                                        with open(os.path.join(dirname,'deletednums'), 'a') as f:
                                            f.write((midnum+'\n'))
                                    except Exception as e:
                                        print(e)
                                    finally:
                                        pass
                                print("Your blog is deleted sucsessfully. We are sorry.")
                                os.sys.exit()
                            else:
                                print("It is a balanced decision. Googbye.")
                                os.sys.exit()
                        else:
                            print("No saved {0} file found. Can't delete your blog.\n{1}".format(file_not_found_msg, \
                                ('Try collect mids before (use `--fetch`) key.' if args.stored_mids else "Save your blog before deleting.")))
                if (args.juick_images or args.external_images or args.new):
                    print("Некоторые из аргументов игнорированы, поскольку выбрана операция удаления блога.")
        except KeyboardInterrupt:
            os.sys.exit(-1)
        else:
            try:
                password = getpass.getpass('@{0} password: '.format(uname))
                if juick.auth(uname, password) == 0:
                    # проверяем, был ли достигнут конец блога
                    eof_reached = True if os.path.isfile(os.path.join(dirname, 'endreached')) else False
                    # попытка всё стереть:)
                    if args.new:
                        try:
                            juick.delete_stored_files(dirname)
                        except Exception as e:
                            raise e
                    #path = os.path.join(dirname, filename)
                    if not os.path.isfile(os.path.join(dirname, 'posts.json')):
                        juick.write_json(os.path.join(dirname, 'posts'),[],'w')
                    temp_post_store = []
                    # счётчики сообщений 
                    if os.path.isfile(os.path.join(dirname, 'savednums')):
                        with open(os.path.join(dirname, 'savednums')) as f:
                            fetched_mid_count = len(f.read().split())
                    else:
                        fetched_mid_count = 0   # собрано всего
                    total_mid_count = 0         # всего сообщений
                    mids = juick.fetch_numbers('http://juick.com/{0}/'.format(uname), dirname=dirname, \
                        eof_reached=eof_reached, delay=DELAY, save_each=SAVE_EACH, \
                        pauses = args.pauses, begin_from = args.begin_from)
                    if not args.new:
                        print("mid count before compare: {0}".format(len(mids)))
                    # Сравниваем списки сохраненных номеров и записанных номеров
                    mids = juick.compare(juick.preview_mid(dirname), mids)
                    # всего сообщений
                    total_mids = len(mids)
                    if not args.new:
                        print("mid count after compare: {0}".format(len(mids)))
                    if not args.fetch:
                        for thread in juick.thread(mids, DELAY, args.pauses):
                            t = json.loads((juick._session.get(thread[0]).content.replace(b'\n', b'').replace(b'\r', b'')).decode(encoding='UTF-8'))
                            # Обработка изображений
                            if args.juick_images or args.external_images:
                                if args.juick_images:
                                    for item in t:
                                        image_downloader.get_juick_image(item, DELAY, imagedir_name) 
                                # если выбрана опция выкачки сторонних изображений
                                if args.external_images:
                                    for item in t:
                                        image_downloader.get_external_image(item, DELAY, external_imagedir_name)
                            t[0]['friends'], t[0]['readonly'] = thread[1], thread[2]
                            fetched_mid_count += 1
                            total_mid_count += 1
                            print('Сообщений в текущей порции: {0}'.format(fetched_mid_count))
                            print('Всего сообщений: {0}'.format(total_mid_count))
                            temp_post_store.append(t)
                            print("Posts done: {0} / Posts all: {1} ({2:.2f}%)".format(total_mid_count, \
                                total_mids, total_mid_count/total_mids*100))
                            # тут запись постов в файл
                            # запись в файл столько, сколько определено в save-each
                            if (fetched_mid_count) == SAVE_EACH or (total_mids - total_mid_count) < SAVE_EACH:
                                try:
                                    print('Сообщений в текущей порции: {0}'.format(fetched_mid_count))
                                    juick.append_json(os.path.join(dirname, 'posts.json'), temp_post_store)
                                    if not os.path.isfile(os.path.join(dirname,'savednums')):
                                        open(os.path.join(dirname,'savednums'), 'a').close()
                                    with open(os.path.join(dirname, 'savednums'), 'r') as f:
                                        saved_list = f.readlines()
                                        for t in temp_post_store:
                                            saved_list.append("{0}\n".format(t[0]['mid']))
                                        saved_list.sort()
                                    with open(os.path.join(dirname, 'savednums'), 'w') as f:
                                        for item in saved_list:
                                            f.write(item)
                                    fetched_mid_count = 0
                                    temp_post_store = []
                                finally:
                                    pass
            except KeyboardInterrupt:
                os.sys.exit(-1)
    else:
        if args.delete or args.fetch or args.stored_mids:
            logging.warning ("Вы не можете использовать эти опциии без авторизации.")
            os.sys.exit()
        else:
            #------------------------------------------------------------------
            #       Получение публичных постов
            #------------------------------------------------------------------
            print("Non-authorized fetching selected.")
            api_fetch = SimpleStore(uname, juick=juick, dirname=dirname, \
                delay=DELAY, args=args)
            api_fetch.previous_data_check()
            for mid in api_fetch.fetch_unauthorized_mids():
                try:
                    logging.info("Try fetch #{0}".format(mid))
                    sleep_delay = uniform(DELAY*1.2, DELAY*1.4)
                    if args.pauses:
                        logging.debug("Paused for {0:.2f} s".format(sleep_delay))
                    time.sleep(sleep_delay)
                    thread = \
                        json.loads(((juick._session.get('http://api.juick.com/thread?mid={0}'.format(mid))).content.replace(b'\n', b'').replace(b'\r', b'')).decode(encoding='UTF-8'))
                    # print ("T",t)
                    # Обработка изображений
                    if args.juick_images or args.external_images:
                        if args.juick_images:
                            for item in thread:
                                image_downloader.get_juick_image(item, DELAY, imagedir_name) 
                        # если выбрана опция выкачки сторонних изображений
                        if args.external_images:
                            for item in thread:
                                image_downloader.get_external_image(item, DELAY, external_imagedir_name)
                    juick.append_json(os.path.join(dirname,'posts.json'), [thread])
                    with open(os.path.join(dirname,'savednums'), 'a') as savednums_file:
                        savednums_file.write("{0}\n".format(mid))
                except Exception as e:
                    raise e
                except KeyboardInterrupt:
                    os.sys.exit(-1)

if __name__ == '__main__':
    main()